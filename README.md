# assignment-6-shanks2710
# Assignment 6
This project mimics the behavior of image-croppers that are available over the internet. It displays capabilities of an image cropper wherein the user can resize a grid to see the exact area being cropped.
Additionally, the user can see a preview of the cropped image on click of a button.

# Prerequisites
This project requires npm to install plugins for ESDoc and to run commands to generate the documentation using ESDoc.
It uses features of HTML, CSS and JavaScript and can be run on any browser like Google Chrome, Mozilla Firefox and Microsoft Edge.

# Built With
Visual Source Code - IDE to create project.
ESDoc: Used to generate documentation for the project

# Run
Steps:
1) Clone the GitHub repo: https://github.com/neu-mis-info6150-spring-2019/assignment-6-shanks2710.git
2) Open Visual Source Code and Select File->Open Folder
3) Navigate to the respective folder and Click Select Folder
4) Go to Terminal Section
5) Navigate to src folder with command: cd src
6) Run the command to run the project: npm install esdoc esdoc-standard-plugin
7) Run the command to initialize project: npm init
8) Run the command to create documentation for the project via ESDoc plugin: npm run doc
9) Documentation for the project is created under the 'dist/esdoc' folder.
10) Navigate to file assignment6.html and open it in browser

# Author
Shashank Sharma, NU-ID: 001415411

# Acknowledgments
Prof. Amuthan Arulraj and all TAs
