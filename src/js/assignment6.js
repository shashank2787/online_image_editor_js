/**
   * @param none
   * Desc: This method is invoked when the Crop button is clicked. Based on the state of the button, the relevant items are displayed or hidden
   */
function btnClicked() {
    console.log('btnClicked');
    //Manipulate the state of the crop-button, Crop_resizable and Crop-Grid areas
    document.getElementById("cropBtn").style.opacity = (document.getElementById("cropBtn").style.opacity == "0.5" ? "1" : "0.5");
    document.getElementById("mainDiv").style.opacity = (document.getElementById("mainDiv").style.opacity == "0.5" ? "0" : "0.5");
    document.getElementById("gridDiv").style.opacity = (document.getElementById("gridDiv").style.opacity == "0.5" ? "0" : "0.5");
    if (document.getElementById("cropBtn").style.opacity == "0.5") {
        //Display the crop-elements to crop the image
        showCropElements();
    }
    else {
        //Crop the image as per the dimensions of the crop-resizable area
        const element = document.querySelector('.resizable');
        const elementParent = document.querySelector('.imgDivStyle');
        pHeight=parseFloat(getComputedStyle(elementParent, null).getPropertyValue('height').replace('px', ''));
        pWidth=parseFloat(getComputedStyle(elementParent, null).getPropertyValue('width').replace('px', ''));
        eHeight=parseFloat(getComputedStyle(element, null).getPropertyValue('height').replace('px', ''));
        eWidth=parseFloat(getComputedStyle(element, null).getPropertyValue('width').replace('px', ''));
        //Set top,right, bottom and left to crop the image 
        crop_top = element.getBoundingClientRect().top-100+'px';
        crop_right = (pWidth-((element.getBoundingClientRect().left-10)+eWidth))+'px';
        crop_bottom = (pHeight-((element.getBoundingClientRect().top-100)+eHeight))+'px';
        crop_left = element.getBoundingClientRect().left-10+'px';
        //Get the image-element and crop it
        const img = document.getElementById('myImg');
        img.style.clipPath = 'inset('+crop_top+' '+crop_right+' '+crop_bottom+' '+crop_left+')';
    }
}
/**
   * @param none
   * Desc: This method displays the elements required to crop the image. Also, it attaches event-listeners on elements to handle resizing of Crop-Resizable Area 
   */
function showCropElements() {
    //Get the crop elements
    const grid = document.querySelector('.grid');
    const element = document.querySelector('.resizable');
    const resizers = document.querySelectorAll('.resizable .resizer');
    //initialize the control variables for crop behavior
    const minimum_size = 20;
    let original_width = 0;
    let original_height = 0;
    let original_x = 0;
    let original_y = 0;
    let original_mouse_x = 0;
    let original_mouse_y = 0;
    //Loop through the resizers and register the event-listeners
    for (let i = 0; i < resizers.length; i++) {
        const currentResizer = resizers[i];
        currentResizer.addEventListener('mousedown', function initListeners(e) {
            e.preventDefault()
            //Update the control variables
            original_width = parseFloat(getComputedStyle(element, null).getPropertyValue('width').replace('px', ''));
            original_height = parseFloat(getComputedStyle(element, null).getPropertyValue('height').replace('px', ''));
            original_x = element.getBoundingClientRect().left;
            original_y = element.getBoundingClientRect().top;
            original_mouse_x = e.pageX;
            original_mouse_y = e.pageY;
            //Added event-listeners
            window.addEventListener('mousemove', resizeCropArea)
            window.addEventListener('mouseup', stopResize)
        })

        //This method controls the dynamic resizing of the crop-area when the user presses and drags the mouse
        function resizeCropArea(e) {
            //Each 'if' condition refers to one of the elements used to resize the grid
            if (currentResizer.classList.contains('bottom-right')) {
                if (e.pageX > 10 && e.pageX < 1005 && e.pageY > 99 && e.pageY < 599) {
                    const width = original_width + (e.pageX - original_mouse_x);
                    const height = original_height + (e.pageY - original_mouse_y)
                    if (width > minimum_size) {
                        element.style.width = width + 'px'
                        grid.style.width = width + 'px'
                    }
                    if (height > minimum_size) {
                        element.style.height = height + 'px'
                        grid.style.height = height + 'px'
                    }
                }
            }
            else if (currentResizer.classList.contains('bottom-left')) {
                if (e.pageX > 10 && e.pageX < 1005 && e.pageY > 99 && e.pageY < 599) {
                    const height = original_height + (e.pageY - original_mouse_y)
                    const width = original_width - (e.pageX - original_mouse_x)
                    if (height > minimum_size) {
                        element.style.height = height + 'px'
                        grid.style.height = height + 'px'
                    }
                    if (width > minimum_size) {
                        element.style.width = width + 'px'
                        element.style.left = original_x + (e.pageX - original_mouse_x) + 'px'
                        grid.style.width = width + 'px'
                        grid.style.left = original_x + (e.pageX - original_mouse_x) + 'px'
                    }
                }
            }
            else if (currentResizer.classList.contains('top-right')) {
                if (e.pageX > 10 && e.pageX < 1005 && e.pageY > 99 && e.pageY < 599) {
                    const width = original_width + (e.pageX - original_mouse_x)
                    const height = original_height - (e.pageY - original_mouse_y)
                    if (width > minimum_size) {
                        element.style.width = width + 'px'
                        grid.style.width = width + 'px'
                    }
                    if (height > minimum_size) {
                        element.style.height = height + 'px'
                        element.style.top = original_y + (e.pageY - original_mouse_y) + 'px'
                        grid.style.height = height + 'px'
                        grid.style.top = original_y + (e.pageY - original_mouse_y) + 'px'
                    }
                }
            }
            else if (currentResizer.classList.contains('top-left')) {
                if (e.pageX > 10 && e.pageX < 1005 && e.pageY > 99 && e.pageY < 599) {
                    const width = original_width - (e.pageX - original_mouse_x)
                    const height = original_height - (e.pageY - original_mouse_y)
                    if (width > minimum_size) {
                        element.style.width = width + 'px'
                        element.style.left = original_x + (e.pageX - original_mouse_x) + 'px'
                        grid.style.width = width + 'px'
                        grid.style.left = original_x + (e.pageX - original_mouse_x) + 'px'
                    }
                    if (height > minimum_size) {
                        element.style.height = height + 'px'
                        element.style.top = original_y + (e.pageY - original_mouse_y) + 'px'
                        grid.style.height = height + 'px'
                        grid.style.top = original_y + (e.pageY - original_mouse_y) + 'px'
                    }
                }
            }
            else if (currentResizer.classList.contains('top-middle')) {
                if (e.pageX > 10 && e.pageX < 1005 && e.pageY > 99 && e.pageY < 599) {
                    const height = original_height - (e.pageY - original_mouse_y)
                    if (height > minimum_size) {
                        element.style.height = height + 'px'
                        element.style.top = original_y + (e.pageY - original_mouse_y) + 'px'
                        grid.style.height = height + 'px'
                        grid.style.top = original_y + (e.pageY - original_mouse_y) + 'px'
                    }
                }
            }
            else if (currentResizer.classList.contains('bottom-middle')) {
                if (e.pageX > 10 && e.pageX < 1005 && e.pageY > 99 && e.pageY < 599) {
                    const height = original_height + (e.pageY - original_mouse_y)
                    if (height > minimum_size) {
                        element.style.height = height + 'px'
                        grid.style.height = height + 'px'
                    }
                }
            }
            else if (currentResizer.classList.contains('right-middle')) {
                if (e.pageX > 10 && e.pageX < 1005 && e.pageY > 99 && e.pageY < 599) {
                    const width = original_width + (e.pageX - original_mouse_x)
                    if (width > minimum_size) {
                        element.style.width = width + 'px'
                        grid.style.width = width + 'px'
                    }
                }
            }
            else if (currentResizer.classList.contains('left-middle')) {
                if (e.pageX > 10 && e.pageX < 1005 && e.pageY > 99 && e.pageY < 599) {
                    const width = original_width - (e.pageX - original_mouse_x)
                    if (width > minimum_size) {
                        element.style.width = width + 'px'
                        element.style.left = original_x + (e.pageX - original_mouse_x) + 'px'
                        grid.style.width = width + 'px'
                        grid.style.left = original_x + (e.pageX - original_mouse_x) + 'px'
                    }
                }
            }
        }

        function stopResize() {
            window.removeEventListener('mousemove', resizeCropArea)
        }
    }
    //Add event-listener to remove all events when window is unloaded
    window.addEventListener("beforeunload", removeListeners);
}

/**
   * @param {!event} e - this is the event that is triggered when the winodw-unload action is performed
   * Desc: This method removes all event-listeners from elements
   */
function removeListeners(e) {
    const resizers = document.querySelectorAll('.resizable .resizer')
    for (let i = 0; i < resizers.length; i++) {
        const currentResizer = resizers[i];
        elClone = currentResizer.cloneNode(true);
        currentResizer.parentNode.replaceChild(elClone, currentResizer);
    }
}



